﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1.Contract;
using Lab1.Implementation;

namespace Lab1.Implementation
{
    public class KosmoFacet :IMezczyzna, IKosmita
    {
        public string PrzedstawSie()
        {
            return "Tutaj kosmita";
        }
        string IKosmita.PrzedstawSie()
        {
            return "zw1";
        }
        string ICzlowiek.PrzedstawSie()
        {
            return "zw2";
        }
        string IMezczyzna.CzyZarosniety()
        {
            return "Tak";
        }
        public string PokazZnak()
        {
            return "V";
        }
        
    }
}
