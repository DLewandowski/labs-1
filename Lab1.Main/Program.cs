﻿using System;
using System.Collections.Generic;
using System.Collections;
using Lab1.Contract;
using Lab1.Implementation;


namespace Lab1.Main
{
    
    public class Program
    {
        public ICollection<ICzlowiek> Metoda()
        {
            IList<ICzlowiek> lista = new List<ICzlowiek>();
            lista.Add(new Babeczka());
            lista.Add(new Facet());
            lista.Add(new Babeczka());
            return lista;
        }

        public void Metoda2(ICollection<ICzlowiek> lista)
        {
            foreach (var item in lista)
            {
                item.PrzedstawSie();
                
            }
        }

      
        
        static void Main(string[] args)
        {
            ICzlowiek cz = new KosmoFacet();
            Console.WriteLine(cz.PrzedstawSie());
            IMezczyzna k = new KosmoFacet();
            Console.WriteLine(k.PrzedstawSie());
            IKosmita kos = new KosmoFacet();
            Console.WriteLine(kos.PrzedstawSie());

            Console.ReadKey();
            

        }
    }
}
