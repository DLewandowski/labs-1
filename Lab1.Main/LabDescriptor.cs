﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab1.Contract;
using Lab1.Implementation;

namespace Lab1.Main
{
    public struct LabDescriptor
    {
        #region P1

        public static Type IBase = typeof(ICzlowiek);
        
        public static Type ISub1 = typeof(IMezczyzna);
        public static Type Impl1 = typeof(Facet);
        
        public static Type ISub2 = typeof(IKobieta);
        public static Type Impl2 = typeof(Babeczka);
        
        
        public static string baseMethod = "PrzedstawSie";
        public static object[] baseMethodParams = new object[] { };

        public static string sub1Method = "CzyZarosniety";
        public static object[] sub1MethodParams = new object[] { };

        public static string sub2Method = "RozmiarStanika";
        public static object[] sub2MethodParams = new object[] { };

        #endregion

        #region P2

        public static string collectionFactoryMethod = "Metoda";
        public static string collectionConsumerMethod = "Metoda2";

        #endregion

        #region P3

        public static Type IOther = typeof(IKosmita);
        public static Type Impl3 = typeof(KosmoFacet);

        public static string otherCommonMethod = "PrzedstawSie";
        public static object[] otherCommonMethodParams = new object[] { };

        #endregion
    }
}
